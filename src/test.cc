#include <gtest/gtest.h>
#include "factorial.h"

TEST(TestFactorial) {
  EXPECT_EQ(factorial(0));
  EXPECT_EQ(factorial(-1));
  EXPECT_EQ(factorial(4));
  EXPECT_EQ(factorial(5));
  EXPECT_EQ(factorial(120));
}
